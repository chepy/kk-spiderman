# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from spiderman.items import ArticleItem

class SpidermanPipeline(object):
    def process_item(self, item, spider):
        return item

class MySqlPipeline(object):
	def process_item(self, item, spider):
		item.SaveDb()
		return item
# Scrapy settings for spiderman project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'spiderman'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['spiderman.spiders']
NEWSPIDER_MODULE = 'spiderman.spiders'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

ITEM_PIPELINES = ['spiderman.pipelines.MySqlPipeline']



DB_HOST = '127.0.0.1:3306'
DB_NAME = 'kk'
DB_USER = 'root'
DB_PASSWORD = ''

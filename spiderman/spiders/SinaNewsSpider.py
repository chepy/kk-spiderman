#coding=utf-8
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from spiderman.items import ArticleItem

class SinaNewsSpider(CrawlSpider):
	name = "SinaNewsSpider"
	allowed_domains = [
		"news.sina.com.cn",
		"ent.sina.com.cn",
		"sports.sina.com.cn"
	]
	start_urls = [
		"http://news.sina.com.cn"
	]
	rules = (
        Rule(SgmlLinkExtractor(allow='/.*\.shtml'),
            'parse_article',
            follow=True,
        ),
    )
	def parse_article(self, response):
		try:
			hxs = HtmlXPathSelector(response)
			article = ArticleItem()
			# 标题
			article['title'] = hxs.select('//h1[@id="artibodyTitle"]/text() | \
				//h1[@id="artibodyTitle"]/*/text()').extract()[0]
			
			article['url'] = response.url
			
			# 媒体
			article['author'] = hxs.select('//span[@id="media_name"]/text() | \
				//span[@id="media_name"]/*/*/text() | \
				//span[@id="media_name"]/*/text() |\
				//div[@class="from_info"]/*/*/text()').extract()[0]
			
			article['pub_date'] = hxs.select('//span[@id="pub_date"]/text()').extract()[0] # 发布日期
			article['content'] = hxs.select('//div[@id="artibody"]').extract()[0]
			
			return article
			
		except Exception, e:
			# 追加url记录
			logger = open('spider_errors.log', 'a+')
			logger.write('\n%s' % response.url)
			logger.write('\n%s\n' % e)
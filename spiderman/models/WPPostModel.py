from sqlalchemy import Column, Integer, String,Text
from spiderman.models import BaseModel


# For Wordpress Post Model
class WPPostModel(BaseModel):
	
	__tablename__ = 'wp_posts'
	ID = Column(Integer, primary_key=True)
	post_author = Column(Integer, default=1)
	post_title = Column(String(255))
	post_content = Column(Text)
	post_status = Column(String(255), default="publish")
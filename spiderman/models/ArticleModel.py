from sqlalchemy import Column, Integer, String,Text
from spiderman.models import BaseModel

class ArticleModel(BaseModel):
	
	__tablename__ = 'articles'
	
	id = Column(Integer, primary_key=True)
	url = Column(String(255), unique=True)
	title = Column(String(255))
	content = Column(Text)
	author = Column(String(255))
	pub_date = Column(String(255))
	
	created_time = Column(String(255))
	updated_time = Column(String(255))
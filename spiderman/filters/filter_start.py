#coding=utf-8
"""
To Wordpress Database articles.
"""
import os
os.sys.path.append('../../')

from spiderman.models import WPPostModel, ArticleModel
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from spiderman.filters.SinaNewsFilter import SinaNewsFilter

SRC_DB_HOST = '127.0.0.1:3306'
SRC_DB_NAME = 'kk'
SRC_DB_USER = 'root'
SRC_DB_PASSWORD = ''

DST_DB_HOST = '127.0.0.1:3306'
DST_DB_NAME = 'wp'
DST_DB_USER = 'root'
DST_DB_PASSWORD = ''


# 源mysql Engine，  放置爬虫物
src_db_engine = create_engine( \
	'mysql://%s:%s@%s/%s?charset=utf8' % ( \
		SRC_DB_USER, SRC_DB_PASSWORD, \
		SRC_DB_HOST, SRC_DB_NAME \
	), encoding='utf-8', echo=True)
SrcDbSession = sessionmaker()   # <- For Use
SrcDbSession.configure(bind=src_db_engine)


# 目标引擎，放置wordpress内容
dst_db_engine = create_engine( \
	'mysql://%s:%s@%s/%s?charset=utf8' % ( \
		DST_DB_USER, DST_DB_PASSWORD, \
		DST_DB_HOST, DST_DB_NAME \
	), encoding='utf-8', echo=True)
DstDbSession = sessionmaker()   
DstDbSession.configure(bind=dst_db_engine)

 # For Use  DbInstance
src_db = SrcDbSession()
dst_db = DstDbSession()


#dst_db.add(WPPostModel(post_title="KK title", post_content="kk content"))
#dst_db.commit()
for row in src_db.query(ArticleModel).all():
    count = dst_db.query(WPPostModel).filter_by(post_title=row.title).count()
    if int(count) is 0:
        # 不存在同标题名的
        new_post = WPPostModel(post_title=row.title, post_content=SinaNewsFilter(row.content))
        #print SinaNewsFilter(row.content)
        dst_db.add(new_post)
        
dst_db.commit()  # Do it...
#    print row.title
#for row in dst_db.query(WPPostModel).all():
#    print row.post_title




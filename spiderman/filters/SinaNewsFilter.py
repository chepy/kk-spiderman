#coding=utf-8
import re
import sys 
reload(sys) 
sys.setdefaultencoding('utf8') 

#content = open('test.txt').read()

class SinaNewsFilter:
    patterns = [ \
        r'\/\/.*<!\[CDATA\[', # <![CDATA[
        r'\/\/.*\]\]>',   # // ]]>
        r'<!--(.*?)-->',  # 去掉 注释 
        r'<script[^>]*?>[\s\S]*?<\/script>', # <script>
        r' style=".*?"',  # 标签内style
        '<span>分享到:</span>', 
        r'<span.*?\我\要\评\论<\/a>', 
        r'<div>.*?今日微博热点.*?</div>',
        r'<style>[\s\S]*<\/style>',
        r'<div class="blkComment[\s\S]*?<\/iframe><\/div><\/div>',
        #r'<iframe id=[\s\S]*iframe>',
    ]
    
    def __init__(self, content):
        for pattern in SinaNewsFilter.patterns:
            re_compiler = re.compile(pattern)
            content = re_compiler.sub('', content)
            
        self.filtered_content = content
        
    def __unicode__(self):
        return unicode(self.filtered_content)
        
    def __str__(self):
        return unicode(self.filtered_content)
        



if __name__ == '__main__':
    t = """ \
。</p>

<p><strong>　　伤势过重不幸牺牲</strong></p>

<p>　　可就在这时，一列广州方向驶来的列车飞驰而来，高架桥上警戒的战友大声地叫喊“快回来！快回来！列车马上就要过来了！”周围的群众也跟着大喊起来：“快一点，快一点！”</p>

<p>　　在大家的呼喊声中，姚携炜仍紧紧抓住轻生男子拼命往外拖。指挥员大喊一声“快走”，但疾驰的列车已经从眼前飞驰而过，“嘭”的一声巨响，姚携炜顿时被列车撞倒，摔落在10多米远的护栏上。</p>

<p>　　“阿炜！阿炜！”战友大声地呼唤着他的名字，但满脸鲜血的姚携炜再也听不到战友的声音。头部受伤的姚携炜被紧急送往新塘人民医院抢救。终因伤势过重于当日下午6时40分不幸牺牲。</p> <div class="blkComment otherContent_01">			<p><span id="comment_t_show_top"><a class="blkCommentLinkPost" href="http://comment4.news.sina.com.cn/comment/comment4.html?channel=sh&amp;newsid=1-1-22996147&amp;style=0">欢迎发表评论</a></span><a href="http://comment4.news.sina.com.cn/comment/comment4.html?channel=sh&amp;newsid=1-1-22996147&amp;style=0">我要评论</a></p>			</div>

                                        <div><span><a href="http://weibo.com/" target="_blank" style='background: url("http://timg.sjs.sinajs.cn/t35/style/images/thridpart/tsina_icon_logo16x16.png") no-repeat scroll 0 0 transparent;padding:4px 0 0 20px;'>微博推荐</a> | <a href="http://news.t.sina.com.cn" target="_blank">今日微博热点</a></span>（编辑：SN056）</div>
					<div></div></div>&#13;
    """
    
    print SinaNewsFilter(t)
#print content

#new_file = open('test2.txt', 'w+')
#new_file.write(content)
#coding=utf-8
from scrapy.item import Item, Field
from spiderman.models import ArticleModel
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from spiderman.models import ArticleModel, BaseModel
from spiderman import settings
import time

# mysql Engine
db_engine = create_engine( \
	'mysql://%s:%s@%s/%s?charset=utf8' % ( \
		settings.DB_USER, settings.DB_PASSWORD, \
		settings.DB_HOST, settings.DB_NAME \
	), encoding='utf-8', echo=True)
DbSession = sessionmaker()
DbSession.configure(bind=db_engine)

BaseModel.metadata.create_all(db_engine) # ¥¥Ω® ˝æ›±Ì

db_session = DbSession() # ˝æ›ø‚”√


class BaseItem(Item):
	url = Field()
	def GetDbEngine(self):
		return session
	
	
class ArticleItem(BaseItem):
	title = Field()
	author = Field() # 来源媒体
	content = Field()
	pub_date = Field() # 发布日期
	
	def SaveDb(self):
		# 判断是否存在 
		# SELECT * FROM articles WHERE url = ${URL}
		count = db_session.query(ArticleModel).filter(ArticleModel.url == self['url']).count()
		if int(count) is 0:
			# 添加
			article = ArticleModel(url=self['url'], title=self['title'],content=self['content'],pub_date=self['pub_date'])
			article.created_time = time.time()
			article.updated_time = time.time()
			
			db_session.add(article)
			db_session.commit()
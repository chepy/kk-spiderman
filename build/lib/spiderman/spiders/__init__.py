# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from spiderman.items import ArticleItem

class SinaSpider(CrawlSpider):
	name = "SinaSpider"
	allowed_domains = [
		"news.sina.com.cn"
	]
	start_urls = [
		"http://news.sina.com.cn/"
	]
	rules = (
        Rule(SgmlLinkExtractor(allow='/.*\.shtml'),
            'parse_article',
            follow=True,
        ),
    )
	def parse_article(self, response):
		hxs = HtmlXPathSelector(response)
		article = ArticleItem()
		article['title'] = hxs.select('//h1[@id="artibodyTitle"]/text()').extract()[0]
		article['url'] = response.url
		return article
# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class SpidermanItem(Item):
    # define the fields for your item here like:
    # name = Field()
    pass

	
class ArticleItem(Item):
	title = Field()
	url = Field()
	author = Field()
	content = Field()
	date_time = Field()